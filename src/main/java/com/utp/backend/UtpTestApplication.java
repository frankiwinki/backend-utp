package com.utp.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UtpTestApplication {

	public static void main(String[] args) {
    SpringApplication.run(UtpTestApplication.class, args);
	}

}
