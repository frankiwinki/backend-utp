package com.utp.backend.controllers;

import com.utp.backend.helpers.UserHelper;
import com.utp.backend.models.Note;
import com.utp.backend.models.User;
import com.utp.backend.payload.request.NoteRequest;
import com.utp.backend.payload.response.BadRequestResponse;
import com.utp.backend.payload.response.MessageResponse;
import com.utp.backend.payload.response.NoteResponse;
import com.utp.backend.payload.response.NotesResponse;
import com.utp.backend.repository.NoteRepository;
import com.utp.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/note")
public class NoteController {

    @Autowired
    NoteRepository noteRepository;
    @Autowired
    UserRepository userRepository;

    @GetMapping("/list")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> notes() {
        User user = UserHelper.getCurrentUser(userRepository);
        List<Note> notes = noteRepository.findAllByUserId(user.getId());
        return ResponseEntity.ok(new NotesResponse(notes));
    }

    @GetMapping("/show/{id}")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> note(@PathVariable(value="id") Long id) {
        User user = UserHelper.getCurrentUser(userRepository);
        Note note = noteRepository.findById(id).get();
        if(note.getUser().getId()!=user.getId()){
            return new ResponseEntity("usted no es el creador de está nota", HttpStatus.FORBIDDEN);
        }
        return ResponseEntity.ok(new NoteResponse(note));
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?>  create(@Valid @RequestBody NoteRequest noteRequest, Errors errors) {

        if (errors.hasErrors()) {
            return new ResponseEntity(new BadRequestResponse(errors), HttpStatus.BAD_REQUEST);
        }

        Note note = new Note(noteRequest.getTitle(),
                noteRequest.getDescription(), new Date());
        User user = UserHelper.getCurrentUser(userRepository);
        note.setUser(user);
        noteRepository.save(note);
        return ResponseEntity.ok(new MessageResponse("Note registered successfully!"));
    }
}