package com.utp.backend.helpers;

import com.utp.backend.models.User;
import com.utp.backend.repository.UserRepository;
import com.utp.backend.security.services.UserDetailsImpl;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserHelper {
    public static User getCurrentUser(UserRepository userRepository) {

        UserDetailsImpl obj = (UserDetailsImpl) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        return  userRepository.findById(obj.getId()).get();

    }
}
