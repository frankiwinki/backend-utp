package com.utp.backend.payload.request;

import java.time.LocalDateTime;
import java.util.Set;

import javax.validation.constraints.*;

public class NoteRequest {
    @NotBlank
    @Size(min = 3, max = 50)
    private String title;

    @NotBlank
    @Size(max = 500)
    private String description;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
