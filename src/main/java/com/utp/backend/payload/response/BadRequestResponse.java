package com.utp.backend.payload.response;

import org.springframework.validation.Errors;

import java.util.stream.Collectors;

public class BadRequestResponse {
    private String message;

    public BadRequestResponse(Errors errors) {

        String errorMessage = errors.getFieldErrors()
                .stream()
                .map(fieldError -> fieldError.getField() + ": " + fieldError.getDefaultMessage())
                .collect(Collectors.joining("; "));

        this.message = errorMessage;
    }
    public  void setMessage(String message) {
         this.message = message;
    }
    public String getMessage() {
        return message;
    }



}
