package com.utp.backend.payload.response;

import com.utp.backend.models.Note;

import java.util.Date;

public class NoteResponse {
    private Long id;
    private String title;
    private Long userId;
    private String description;
    private Date creationDate;

    public NoteResponse(Long id, Long userId, String title, String description, Date creationDate) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.description = description;
        this.creationDate = creationDate;
    }
    public NoteResponse(Note note) {
        this.id = note.getId();
        this.userId = note.getUser().getId();
        this.title = note.getTitle();
        this.description = note.getDescription();
        this.creationDate = note.getCreationDate();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
