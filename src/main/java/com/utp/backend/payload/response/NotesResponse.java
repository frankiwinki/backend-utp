package com.utp.backend.payload.response;

import com.utp.backend.models.Note;

import java.util.ArrayList;
import java.util.List;

public class NotesResponse {
    private List<NoteResponse> response = new ArrayList<>() ;

    public NotesResponse(List<Note> notes) {
        for(Note note : notes){
            NoteResponse noteResponse = new NoteResponse(note);
            response.add(noteResponse);
        }
    }
    public void setResponse(List<NoteResponse> response) {
        this.response = response;
    }
    public List<NoteResponse> getResponse() {
        return response;
    }
}
